Embedded code written in C for PIC microprocessor. Program files modify a 
3D array, representing a custom developed electronic LED cube. The software 
is loaded onto the PIC to orchestrate beautiful 3D light shows on the LED cube.