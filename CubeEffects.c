
/*
 * File:   CubeEffects.c
 * Author: joshfadaie
 *
 * Created on June 11, 2014, 3:18 PM
 */


#include "CubeEffects.h"


extern void delay_ms(int ms);
extern void delay_sec(int delay);


// Lights the 8 corners of the cube. The origin is lit twice for reference.
void testCorners(int delay){

    // Layer 0 Corners
    setClrVoxel(0, 0, 0, delay);
    setClrVoxel(3, 3, 0, delay);
    setClrVoxel(3, 0, 0, delay);
    setClrVoxel(0, 3, 0, delay);
    setClrVoxel(0, 0, 0, delay);

    // Layer 3 Corners
    setClrVoxel(0, 0, 3, delay);
    setClrVoxel(3, 3, 3, delay);
    setClrVoxel(3, 0, 3, delay);
    setClrVoxel(0, 3, 3, delay);

}


// Lights the center 2x2x2 Cube
void resetVisual(int delay) {
    int z = 1;

    // Set the inner cube voxels
    for(z = 1; z < 3; z++) {
        setVoxel(1, 1, z);
        setVoxel(1, 2, z);
        setVoxel(2, 1, z);
        setVoxel(2, 2, z);
    }
    delay_ms(delay);
    // Clear the inner cube voxels
   for(z = 1; z < 3; z++) {
        clrVoxel(1, 1, z);
        clrVoxel(1, 2, z);
        clrVoxel(2, 1, z);
        clrVoxel(2, 2, z);
    }

    delay_ms(delay);
    
}



// Lights up each of the LEDs in the outer perimeter starting with (0,0,0), and
// ending with the top layer. Then lights the LEDs in the inner perimeter
// starting with the top layer and ending at the bottom layer.
void circleCube(int delay) {
    int x = 0;
    int y = 0;
    int z = 0;

    // Lights the Outer perimeter
    for (z = 0; z < 4; z++) {

         // Line x0
         for (y = 0; y < 4; y++) {
            setClrVoxel(0, y, z, delay);
         }

         // Line y3
         for (x = 1; x < 4; x++) {
            setClrVoxel(x, 3, z, delay);
         }

         // Line x3
         for (y = 2; y >= 0; y--) {
            setClrVoxel(3, y, z, delay);   
         }

         // Line y0
         for (x = 2; x >= 0; x--) {
            setClrVoxel(x, 0, z, delay);
         }

    }

    // Lights the inner perimeter
    for (z = 3; z >=0; z--) {

         // Line x1
         for (y = 1; y < 3; y++) {
            setClrVoxel(1, y, z, delay);
           
         }

         // Line y2
         for (x = 2; x < 3; x++) {
            setClrVoxel(x, 2, z, delay);
          
         }

         // Line x2
         for (y = 1; y >= 1; y--) {
            setClrVoxel(2, y, z, delay);

         }

         // Line y1
         for (x = 1; x >= 1; x--) {
            setClrVoxel(x, 1, z, delay);
         }

    }

    
}


// Creates a 3-D solid 2x2x2 cube inside a 3-D line cube.
void fourDVisual(int delayChange, int delayCube) {

    set3DLineCube();

    delay_sec(delayChange);

    clr3DLineCube();

    lightCube2x2x2(1, 1, 1, delayCube);
    
}


// Tests lines along X and Y Dimension on layer 0 and 2. Tests Z-dimension
// columns in between.
void testLines(int delay) {

    // Test XDim Lines along Layer 0
    setClrLineXDim2(0, 3, 0, 0, 999);
    delay_ms(delay);
    setClrLineXDim2(0, 3, 2, 0, 999);

    delay_ms(delay);

    // Test YDim Lines along Layer 0
    setClrLineYDim2(0, 0, 3, 0, 999);
    delay_ms(delay);
    setClrLineYDim2(2, 0, 3, 0, 999);

    delay_ms(delay);

    // Test Z
    setClrLineZDim2(0, 0, 0, 3, 999);
    delay_ms(delay);
    setClrLineZDim2(3, 0, 0, 3, 999);
    delay_ms(delay);
    setClrLineZDim2(0, 3, 0, 3, 999);
    delay_ms(delay);
    setClrLineZDim2(3, 3, 0, 3, 999);

    delay_ms(delay);

     // Test XDim Lines along Layer 2
    setClrLineXDim2(0, 3, 0, 2, 999);
    delay_ms(delay);
    setClrLineXDim2(0, 3, 2, 2, 999);

    delay_ms(delay);

    // Test YDim Lines along Layer 2
    setClrLineYDim2(0, 0, 3, 2, 999);
    delay_ms(delay);
    setClrLineYDim2(2, 0, 3, 2, 999);
}


// Lights the outer perimeter of the cube. An inner 2x2x2 cube attempts to
// break free.
void cubeEscape(int breakAttempts, int delay) {
    
    int repeats = 0;
    srand(seed);  // Seed random number generator;
    
    int randX;
    int randY;
    int randZ;

    set3DLineCube();

    for (repeats = 0; repeats < breakAttempts; repeats++) {
        
        // Generate random coordinate (ensure coord is not greater than 2x2x2)
        randX = rand() % 3;
        randY = rand() % 3;
        randZ = rand() % 3;

        lightCube2x2x2(randX, randY, randZ, delay);
        set3DLineCube();
    }
    
    clr3DLineCube();

}


// Creates an equilizer graphic with three possible states. State 0:
// Lights four evenly spaced 2x2 columns of random heights. State 1:
// Lights all 16 Z-Columns with random heights. State 2:  // Light a center 2x2
// column, and the rest are 1x1.
void equalizer(int delay, int speed, int repeat, int state) {

    int i = 0;
    int x;
    int y;
    int h1;
    int h2;
    int h3;
    int h4;
  
    srand(seed);


    for (i = 0; i < repeat; i++) {

        // Lights four evenly spaced 2x2 columns of random heights.
        if (state == 0) {

            // Set the four 2x2 platforms
            h1 = rand() % 4;
            setZColumn(0, 0, h1 , 2, 2);
            h2 = rand() % 4;
            setZColumn(0, 2, h2 , 2, 2);
            h3 = rand() % 4;
            setZColumn(2, 0, h3 , 2, 2);
            h4 = rand() % 4;
            setZColumn(2, 2, h4 , 2, 2);

            // Modify the delay
            delay_sec(speed*delay);

             // Clear the four 2x2 platforms
            clrZColumn(0, 0, h1 , 2, 2);
            clrZColumn(0, 2, h2 , 2, 2);
            clrZColumn(2, 0, h3 , 2, 2);
            clrZColumn(2, 2, h4 , 2, 2);

        // Lights all 16 Z-Columns with random heights
        } else if (state == 1) {

            for (x = 0; x < 4; x++) {
                for (y = 0; y < 4; y++) {
                    h1 = rand() % 4;
                    setZColumn(x, y, h1, 1, 1);
                }
            }
            
            // Modify the delay
            delay_sec(speed*delay);

            // Erase image
            clrCube();

        // Light a center 2x2 column, and the rest are 1x1
        } else {
            
            for (x = 0; x < 4; x++) {
                for (y = 0; y < 4; y++) {
                    if ((x < 1 || x > 2) && (y != 1 || y != 2)) {
                        h1 = rand() % 4;
                        setZColumn(x, y, h1, 1, 1);
                    }

                    else {
                        h1 = rand() % 4;
                        setZColumn(1, 1, h1, 2, 2);
                        
                        // Modify the delay
                        delay_sec(speed*delay);

                        clrZColumn(1, 1, h1, 2, 2);
                    }
                }
            }

            h1 = rand() % 4;
            setZColumn(1, 1, h1, 2, 2);

            // Modify the delay
            delay_sec(speed*delay);

            // Erase image
            clrCube();


        }
    }
}

// Creates a visual display that modifies the planes displayed.
void bufferLevels(int delay, int speed, int repeat, int state) {

    int i;
    int x;
    int y;
    int z;

    for (i = 0; i < repeat; i++) {

        // Buffers in the Z direction.
        if (state == 0) {
             for (z = 0; z < 4; z++ ) {
               setZPlane(z);
               delay_sec(speed*delay);
               clrZPlane(z);
             }

        // Buffers in the Y direction.
        } else if (state == 1) {
            for (y = 0; y < 4; y++ ) {
               setYPlane(y);
               delay_sec(speed*delay);
               clrYPlane(y);
             }


        // Buffers in the X direction.
        } else if (state == 2) {
            for (x = 0; x < 4; x++ ) {
               setXPlane(x);
               delay_sec(speed*delay);
               clrXPlane(x);
             }

        
        } else {

            /*
            // Pivot (2, 2)
            setYPlane(2);
            setXYPlane(0, 0, 3, 3, 3);
            setXPlane(2);
            setXYPlane(1, 3, 3, 1, 3);
            setYPlane(2);
            setXYPlane(0, 0, 3, 3, 3);
            setXPlane(2);
            setXYPlane(1, 3, 3, 1, 3);

            // Pivot (1, 2)
            setYPlane(2);
            setXYPlane(0, 1, 2, 3, 3);
            setXPlane(1);
            setXYPlane(0, 3, 3, 0, 3);
            setYPlane(2);
            setXYPlane(0, 1, 2, 3, 3);
            setXPlane(1);
            setXYPlane(0, 3, 3, 0, 3);

            // Pivot (1, 1)
            setYPlane(1);
            setXYPlane(0, 0, 3, 3, 3);
            setXPlane(1);
            setXYPlane(0, 2, 2, 0, 3);
            setYPlane(1);
            setXYPlane(0, 0, 3, 3, 3);
            setXPlane(1);
            setXYPlane(0, 2, 2, 0, 3);

            // Pivot (2, 1)
            setYPlane(1);
            setXYPlane(1, 0, 3, 2, 3);
            setXPlane(2);
            setXYPlane(0, 3, 3, 0, 3);
            setYPlane(1);
            setXYPlane(1, 0, 3, 2, 3);
            setXPlane(2);
            setXYPlane(0, 3, 3, 0, 3);
             */

            rotatePlaneAboutZAxis(2, 2, delay, speed);
            rotatePlaneAboutZAxis(1, 2, delay, speed);
            rotatePlaneAboutZAxis(1, 1, delay, speed);
            rotatePlaneAboutZAxis(2, 1, delay, speed);

        }      
    } 
 }


void rotatePlaneAboutZAxis(int xPiv, int yPiv, int delay, int speed) {
    int i = 0;

    // Display for Pivot (1,1) or (2,2)
    if (xPiv == yPiv) {

        for (i = 0; i < 2; i++) {

            setYPlane(yPiv);
            delay_sec(speed*delay);
            clrYPlane(yPiv);

            setXYPlane(0, 0, 3, 3, 3);
            delay_sec(speed*delay);
            clrXYPlane(0, 0, 3, 3, 3);

            setXPlane(xPiv);
            delay_sec(speed*delay);
            clrXPlane(xPiv);

            setXYPlane(xPiv - 1, yPiv + 1, xPiv + 1, yPiv - 1, 3);
            delay_sec(speed*delay);
            clrXYPlane(xPiv - 1, yPiv + 1, xPiv + 1, yPiv - 1, 3);
            
        }
        
    }

     // For Pivot (1, 2) and (2, 1)
    else {
        for (i = 0; i < 2; i++) {
           
            setYPlane(yPiv);
            delay_sec(speed*delay);
            clrYPlane(yPiv);
            
            setXYPlane(xPiv - 1, yPiv - 1, xPiv + 1, yPiv + 1, 3);
            delay_sec(speed*delay);
            clrXYPlane(xPiv - 1, yPiv - 1, xPiv + 1, yPiv + 1, 3);
            
            setXPlane(xPiv);
            delay_sec(speed*delay);
            clrXPlane(xPiv);
            
            setXYPlane(0, 3, 3, 0, 3);
            delay_sec(speed*delay);
            clrXYPlane(0, 3, 3, 0, 3);
        }
            
    }
}