/* 
 * File:   CubeEffects.h
 * Author: joshfadaie
 *
 * Created on June 24, 2014, 12:09 AM
 */

#ifndef CUBEEFFECTS_H
#define	CUBEEFFECTS_H


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "GlobalVars.h"
#include "BasicVisualFuncs.h"

void testCorners(int delay);
void circleCube(int delay);
void resetVisual(int delay);
void fourDVisual(int delayChange, int delayCube);
void testLines(int delay);
void cubeEscape(int breakAttempts, int delay);
void equalizer(int delay, int speed, int repeat, int state);
void equalizer(int delay, int speed, int repeat, int state);
void bufferLevels(int delay, int speed, int repeat, int state);
void rotatePlaneAboutZAxis(int xPiv, int yPix, int delay, int speed);

#endif	/* CUBEEFFECTS_H */

