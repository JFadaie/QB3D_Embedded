/*
 * File:   BasicVisualFuncs.c
 * Author: joshfadaie
 *
 * Created on June 11, 2014, 3:18 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "BasicVisualFuncs.h"

extern void delay_ms(int ms);
extern void delay_sec(int delay);


// Verifies that the coordinate provided is valid. Each dimension is between
// 0 and 3, otherwise 0 is returned.
int validCoord(int x, int y, int z) {
    int valid = 1;
    if ((x >= 0 && x <= 3)  || (y >= 0 && y <= 3)  || (z >= 0 && z <= 3))
        return valid;
    else
    	return !valid;
}


// Sets the bit value associated with the provided cube coordinate. If the
// provided coordinate is not valid, no bits are set.
void setVoxel(int x, int y, int z) {

    if (validCoord(x, y, z)) {

        cube[z][y] |= (0x01 << x);

    }
}

// Clears the bit value associated with the provided cube coordinate. If the
// provided coordinate is not valid, no bits are set.
void clrVoxel(int x, int y, int z) {

    if (validCoord(x, y, z)) {

        cube[z][y] &= !(0x01 << x);

    }
}

// Sets and then clears the specified voxel after the given amount of delay
// has passed.
void setClrVoxel(int x, int y, int z, int ms) {

     setVoxel(x, y , z);
     delay_ms(ms);
     clrVoxel(x ,y , z);


}

// Creates a 2x2x2 cube with the bottom corner provided. X, Y, and Z must be
// no greater than 2, 2, 2.
void lightCube2x2x2(int x, int y, int z, int delay) {
    
    int zPos;
   
    for (zPos = z; zPos <= (z + 1); zPos++) {

        setVoxel(x, y, zPos);
        setVoxel(x, y + 1, zPos);
        setVoxel(x + 1, y, zPos);
        setVoxel(x + 1, y + 1, zPos);
        
    }
    
    delay_ms(delay);

    for (zPos = z; zPos <= (z + 1); zPos++) {

        // Clear the voxels to conserve power
        clrVoxel(x, y, zPos);
        clrVoxel(x, y + 1, zPos);
        clrVoxel(x + 1, y, zPos);
        clrVoxel(x + 1, y + 1, zPos);

    }
    
}


void lightLineCubeX(int x, int y, int z, int dim, int delay) {


    // Create
    for (z = 1; z <= (z + dim); z++) {


        setVoxel(x, y, z);
        setVoxel(x, y + dim, z);
        setVoxel(x + dim, y, z);
        setVoxel(x + 1, y + 1, z);

        delay_ms(delay);

        // Clear the voxels to conserve power
        clrVoxel(x, y, z + 1);
        clrVoxel(x, y + 1, z + 1);
        clrVoxel(x + 1, y, z + 1);
        clrVoxel(x + 1, y + 1, z + 1);

    } 
}


// Creates a line in the X Dimension
void setClrLineXDim(int x1, int x2, int y, int z) {
    
    int pX = x1;
    for (pX = x1; pX <=x2; pX++) {
        setClrVoxel(pX, y, z, 10);

    }
}


// Creates a line in the Y Dimension
void setClrLineYDim(int x, int y1, int y2, int z) {
    int pY = y1;
    for (pY = y1; pY <=y2; pY++) {
        setClrVoxel(x, pY, z, 10);

    }
}


// Creates a line in the Z Dimension
void setClrLineZDim(int x, int y, int z1, int z2) {
    int pZ = z1;
    for (pZ = z1; pZ <= z2; pZ++) {
        setClrVoxel(x, y, pZ, 10);

    }
}

// Creates a line in the X Dimension
void setClrLineXDim2(int x1, int x2, int y, int z, int delay) {

    setLineXDim(x1, x2, y, z);
    delay_ms(delay);
    clrLineXDim(x1, x2, y, z);
}


// Creates a line in the Y Dimension
void setClrLineYDim2(int x, int y1, int y2, int z, int delay) {
    
    setLineYDim(x, y1, y2, z);
    delay_ms(delay);
    clrLineYDim(x, y1, y2, z);

}


// Creates a line in the Z Dimension
void setClrLineZDim2(int x, int y, int z1, int z2, int delay) {
    
    setLineZDim(x, y, z1, z2);
    delay_ms(delay);
    clrLineZDim(x, y, z1, z2);
}


// Creates a line in the X Dimension
void setLineXDim(int x1, int x2, int y, int z) {

    int pX = x1;
    for (pX = x1; pX <=x2; pX++) {
        setVoxel(pX, y, z);

    }
}


// Creates a line in the Y Dimension
void setLineYDim(int x, int y1, int y2, int z) {
    int pY = y1;
    for (pY = y1; pY <=y2; pY++) {
        setVoxel(x, pY, z);

    }
}


// Creates a line in the Z Dimension
void setLineZDim(int x, int y, int z1, int z2) {
    int pZ = z1;
    for (pZ = z1; pZ <= z2; pZ++) {
        setVoxel(x, y, pZ);

    }
}

// Clears a line in the X Dimension
void clrLineXDim(int x1, int x2, int y, int z) {
    int pX = x1;
    for (pX = x1; pX <=x2; pX++) {
        clrVoxel(pX, y, z);

    }

}

// Clears a line in the Y Dimension
void clrLineYDim(int x, int y1, int y2, int z) {
    int pY = y1;
    for (pY = y1; pY <=y2; pY++) {
        clrVoxel(x, pY, z);

    }
    
}

// Clears a line in the Z Dimension
void clrLineZDim(int x, int y, int z1, int z2) {
    int pZ = z1;
    for (pZ = z1; pZ <= z2; pZ++) {
        clrVoxel(x, y, pZ);

    }

    
}


// Lights a plane along the X-axis.
void setXPlane(int x) {
    int y;
    int z;

    for (z = 0; z < 4; z++) {

        for (y = 0; y < 4; y++) {

            cube[z][y] |= ((0x01) << x);
            
        }
    }


}

// Lights a plane along the Y-axis.
void setYPlane(int y) {
    int z;

    for (z = 0; z < 4; z++) {

        cube[z][y] = (0x0f);

    }
}

// Lights a plane along the Z-axis.
void setZPlane(int z) {
    int y;

    for (y = 0; y < 4; y++) {
        cube[z][y] = (0x0f);
    }
}


// Clear a plane along the X-axis.
void clrXPlane(int x) {
    int y;
    int z;

    for (z = 0; z < 4; z++) {

        for (y = 0; y < 4; y++) {

            cube[z][y] &= !(0x01 << x);

        }
    }


}

// Clears a plane along the Y-axis.
void clrYPlane(int y) {
    int z;

    for (z = 0; z < 4; z++) {

        cube[z][y] = (0x00);

    }
}

// Clears a plane along the Z-axis.
void clrZPlane(int z) {
    int y;

    for (y = 0; y < 4; y++) {
        cube[z][y] = 0x00;
    }
}

// Creates a diagonal plane in the x and y axis with height z. The x-y line
// starts at point 1 (x1, y1) and ends at point 2 (x2, y2)
void setXYPlane(int x1, int y1, int x2, int y2, int z) {
    
    int xPos = x1;
    int yPos = y1;

    for (xPos = x1; xPos <= x2; xPos++) {
        setZColumn(xPos, yPos, z, 1, 1);
        if (y2 >= y1)
            yPos++;
        else
            yPos--;
    }
}


// Clears a diagonal plane in the x and y axis with height z. The x-y line
// starts at point 1 (x1, y1) and ends at point 2 (x2, y2)
void clrXYPlane(int x1, int y1, int x2, int y2, int z) {

    int xPos = x1;
    int yPos = y1;

    for (xPos = x1; xPos <= x2; xPos++) {
        clrZColumn(xPos, yPos, z, 1, 1);
        if (y2 >= y1)
            yPos++;
        else
            yPos--;
    }
}


// Lights the LEDCube to form a 3D line cube.
void set3DLineCube() {
    
    // Layer 0 Square Perimeter
    setLineXDim(0, 3, 0, 0);
    setLineXDim(0, 3, 3, 0);
    setLineYDim(0, 0, 3, 0);
    setLineYDim(3, 0, 3, 0);

    // Light columns along the edges of cube
    setLineZDim(0, 0, 0, 3);
    setLineZDim(3, 0, 0, 3);
    setLineZDim(0, 3, 0, 3);
    setLineZDim(3, 3, 0, 3);

    // Layer 3 Square Perimeter
    setLineXDim(0, 3, 0, 3);
    setLineXDim(0, 3, 3, 3);
    setLineYDim(0, 0, 3, 3);
    setLineYDim(3, 0, 3, 3);
}

// Clears a 3D line representation of a cube.
void clr3DLineCube() {

    // Clear Layer 0 Square Perimeter
    clrLineXDim(0, 3, 0, 0);
    clrLineXDim(0, 3, 3, 0);
    clrLineYDim(0, 0, 3, 0);
    clrLineYDim(3, 0, 3, 0);

    // Clear columns along the edges of cube
    clrLineZDim(0, 0, 0, 3);
    clrLineZDim(3, 0, 0, 3);
    clrLineZDim(0, 3, 0, 3);
    clrLineZDim(3, 3, 0, 3);

    // Clear Layer 3 Square Perimeter
    clrLineXDim(0, 3, 0, 3);
    clrLineXDim(0, 3, 3, 3);
    clrLineYDim(0, 0, 3, 3);
    clrLineYDim(3, 0, 3, 3);


    
}

// Lights a column positioned at (x, y) with height "height", and width and
// length dimX, and dimY, respectively.
void setZColumn(int x, int y, int height, int dimX, int dimY) {
   int xPos;
   int yPos;
    
   for (xPos = x; xPos < (x + dimX); xPos++) {
       for (yPos = y; yPos < (y + dimY); yPos++) {
            setLineZDim(xPos, yPos, 0, height);
       }
   }
}


// Clears a column positioned at (x, y) with height "height", and width and
// length dimX, and dimY, respectively.
void clrZColumn(int x, int y, int height, int dimX, int dimY) {
   int xPos;
   int yPos;

   for (xPos = x; xPos < (x + dimX); xPos++) {
       for (yPos = y; yPos < (y + dimY); yPos++) {
            clrLineZDim(xPos, yPos, 0, height);
       }
   }


    
}

// Clears the cube of all images
void clrCube() {
    int y;
    int z;
    
    for (z = 0; z < 4; z++) {
        for (y = 0; y < 4; y++) {
            cube[z][y] = 0x00;
        }
    }
}