/* 
 * File:   BasicVisualFuncs.h
 * Author: joshfadaie
 *
 * Created on June 23, 2014, 11:41 PM
 */

#ifndef BASICVISUALFUNCS_H
#define	BASICVISUALFUNCS_H

#include "GlobalVars.h"


int validCoord(int x, int y, int z);
void setVoxel(int x, int y, int z);
void clrVoxel(int x, int y, int z);
uint8_t getVoxel(int x, int y, int z);
void setClrVoxel(int x, int y, int z, int ms);

void setClrLineXDim(int x1, int x2, int y, int z);
void setClrLineYDim(int x, int y1, int y2, int z);
void setClrLineZDim(int x, int y, int z1, int z2);

void setClrLineXDim2(int x1, int x2, int y, int z, int delay);
void setClrLineYDim2(int x, int y1, int y2, int z, int delay);
void setClrLineZDim2(int x, int y, int z1, int z2, int delay);

void setLineXDim(int x1, int x2, int y, int z);
void setLineYDim(int x, int y1, int y2, int z);
void setLineZDim(int x, int y, int z1, int z2);

void clrLineXDim(int x1, int x2, int y, int z);
void clrLineYDim(int x, int y1, int y2, int z);
void clrLineZDim(int x, int y, int z1, int z2);

void setZColumn(int x, int y, int height, int dimX, int dimY);
void clrZColumn(int x, int y, int height, int dimX, int dimY);

void setXPlane(int x);
void setYPlane(int y);
void setZPlane(int z);

void clrXPlane(int x);
void clrYPlane(int y);
void clrZPlane(int z);

void setXYPlane(int x1, int y1, int x2, int y2, int z);
void clrXYPlane(int x1, int y1, int x2, int y2, int z);

void lightCube2x2x2(int x, int y, int z, int delay);
void lightLineCubeX(int x, int y, int z, int dim, int delay);

void set3DLineCube(void);
void clr3DLineCube(void);

void clrCube(void);


#endif	/* BASICVISUALFUNCS_H */

