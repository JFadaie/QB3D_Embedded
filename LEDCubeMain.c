/* 
 * File:   LEDCubeMain.c
 * Author: joshfadaie
 *
 * Created on June 11, 2014, 3:18 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <xc.h>
//#include <p18cxxx.h>
//#include <p18f452.h>
#include <string.h>
#include "GlobalVars.h"
#include "BasicVisualFuncs.h"
#include "CubeEffects.h"


/*
    EXPLANATION OF DESIGN:
 *
 * The LED cube is split into a 3-dimensional coordinate system.  This
 * coordinate system is abstracted into a two dimensional array where the first
 * and second dimension are the y and z coordinates respectively. The byte
 * assigned to these indicies determines the state of the LED at each coordinate.
 * The first four bits of the byte correspond to the x0, x1, x2, and x3 LEDs
 * in the coordinate system below. The value of these bits, either a 0 or 1
 * determine the state of the LED, being ON or OFF respectively.
 * 
 *
 *              | z0
 *              |
 *              | z1
 *              |
 *              | z2
 *              |
 *              | z3
 *        x0 /     \ y0
 *      x1 /         \ y1
 *    x2 /             \ y2
 *  x3 /                 \ y3
 *
 */


//------------------- CONFIGURATION SETTINGS ----------------- //

/**
 * Oscillator is configured as HSHP (18 MHz)
 * Watch dog timer is disabled
 * Extended instruction mode is disabled
 * Oscillator switch over is disabled
 * Low voltage programming is disabled
 */

#pragma config OSC = HS, WDT = OFF, LVP = OFF, CP0 = OFF, CP1 = OFF, CP2 = OFF, CP3 = OFF


// ------------- PREPROCESSOR DIRECTIVES ------------- //
#define _XTAL_FREQ      18.432e6  // Oscillator frequency for _delay()

#define RESET_SW        PORTCbits.RC2  // Reset switch (1: GND/Play, 2: Power/Reset)
#define RED_STATUS      LATCbits.LATC0  // Red Status LED  (Reset)
#define GREEN_STATUS    LATCbits.LATC1  // Green Status LED (Play)



// ------------- FUNCTION DECLARATIONS ------------- //

void IO_Init(void);
void TMR0Init(void);
void InterruptInit(void);

void setLEDGrid1(uint8_t setBits);
void setLEDGrid2(uint8_t setBits);
void setLEDLayers(uint8_t setBits);

void cubeScheduler(int visFuncNum);

void delay_ms(int ms);
void delay_sec(int delay);

//void interrupt cubeUpdateISR(void);  // Interrupt service routine



// ------------- VISUAL EFFECTS INCLUDES ------------- //



// ------------- BEGIN MAIN PROGRAM ------------- //

int main(void) {

    int visFuncNum = 1;  // Determines which visual effect is playing
    
    // Initialize GPIO pins, timer, and interrupt
    IO_Init();
    InterruptInit();
    TMR0Init();
    

    // The program begins
    while (1) {

        // Reset Logic
        if (RESET_SW == 1) {
            visFuncNum = 0;  // Go to reset display
            resetFlag = 1;   // set reset flag
            RED_STATUS = 1;  // light up red status LED
        } else {
            RED_STATUS = 0;  // turn off red status LED
        }


        // Power ON Logic
        if (RESET_SW == 0 && resetFlag == 1) {
            resetFlag = 0;   // clear reset Flag
            RED_STATUS = 0;  // turn off red status LED
            GREEN_STATUS = 1; // turn on green status LED
            delay_sec(4000);    // delay for a few seconds (maybe need another interrupt)
            GREEN_STATUS = 0;
        }

        cubeScheduler(visFuncNum);

        visFuncNum++;
        if (visFuncNum == 7) // Only two visual displays currently
          visFuncNum = 1;
    }
    

    return (EXIT_SUCCESS);
}


//------------FUNCTION DEFINITIONS ----------------------------------------------------------------


// High Priority Interrupt. Used to update the cube visually.
void interrupt cubeUpdateISR(void)
{


    if(INTCONbits.TMR0IF) {         // Verify interrupt flag is set
        
        // Turn off layers to avoid viewing the visual update
        setLEDLayers(0x00);

        uint8_t grid1Param = (0x0f & cube[currentLayer][0]) |
                             (0xf0 & (cube[currentLayer][1] << 4));
        uint8_t grid2Param = (0x0f & cube[currentLayer][2]) |
                             (0xf0 & (cube[currentLayer][3] << 4));


        // Turn on Grid 1 and 2 LEDs
        setLEDGrid1(grid1Param);
        setLEDGrid2(grid2Param);


        setLEDLayers(0x01 << currentLayer);

        currentLayer++;
        currentLayer %= 4;  // Reset when currentLayer equals 4


        /*
         Calculations:
            FPS = (Instr. Clk)/[(Timer')(Prescaler)(4-layers)]
            where Timer' equals two complement of calc. value

            Use prescaler = 1:32, and timer counts equals 105
         */
        // Reset the timer
        TMR0L = 0x68;  // 105 in decimal
        
        INTCONbits.TMR0IF = 0;            // Reset interrupt flag

        seed++;
    }

    seed += 3;

    if (seed >= 400)
        seed = seed - 399;
}



// Provides a scheduler to call on the desired visual functions
void cubeScheduler(int visFuncNum) {
    int delay = 200;
    int i = 0;
    
    switch (visFuncNum) {

        // Reset Case
        case 0:
            resetVisual(500);  // 500 millisecond delay
            break;

        // First visual function
        case 1:
            
            for (delay = 70; delay >= 10; delay -= 20 )
                circleCube(delay); // 500 millisecond delay
            circleCube(10);
            break;
            
        case 2:
            testCorners(500);
            break;
        case 3:
            for (delay = 1500; delay >= 250; delay -= 75 )
                fourDVisual(delay, 300);
            fourDVisual(200, 300);
            break;
        case 4:
            testLines(200);
            break;
        case 5:
            cubeEscape(20, 500);
            break;
        case 6:
            for (i = 0; i < 3; i++)
                bufferLevels(100, 1, 10, i);
            bufferLevels(100, 1, 4, 3);
            break;
        case 7:
            for (i = 0; i < 3; i++)
                equalizer(100, 1, 10, i);
            break;
        default:
            visFuncNum = 0;
            break;


    }

}



// Initializes the GPIO for the cube layers, status pins, and LEDs as outputs,
// except for RC2 which controls reset.
void IO_Init() {

    // Initialize LED Grid 1
    TRISAbits.RA0 = 0;
    TRISAbits.RA1 = 0;
    TRISAbits.RA2 = 0;
    TRISAbits.RA3 = 0;
    TRISAbits.RA4 = 0;
    TRISEbits.RE0 = 0;
    TRISEbits.RE1 = 0;
    TRISEbits.RE2 = 0;

    // Initialize LED Grid 2
    TRISD = 0x00;

    // Initialize LED Layers
    TRISBbits.RB2 = 0;
    TRISBbits.RB3 = 0;
    TRISBbits.RB4 = 0;
    TRISBbits.RB5 = 0;


    // Initialize Status Pins
    TRISCbits.RC0 = 0;  // reset LED
    TRISCbits.RC1 = 0;  // program play LED
    TRISCbits.RC2 = 1;  // determines between reset and play
    
}


// Initializes Timer 0 as 8 bit
void TMR0Init() {
    INTCONbits.TMR0IE = 1;  // Enables the timer zero interrupt
    INTCON2bits.TMR0IP = 1;  // Sets the TMR0 to high priority
    T0CONbits.T0CS = 0;      // Clock source is internal instr cycle clock (CLK0)
    T0CONbits.T08BIT = 1;    // 8-bit timer
    T0CONbits.TMR0ON = 1;    // Enable TMR0
    T0CONbits.PSA = 0;       // Timer0 prescaler assigned
    T0CONbits.T0PS = 4;      // 1:32 prescaler
    INTCONbits.TMR0IF = 0;
}


// Initializes the interrupt register settings
void InterruptInit() {

    /*RCONbits.IPEN = 1;    // Enables interrupt priority
    INTCONbits.GIEH = 1;  // Enables all unmasked or high priority INT
    INTCONbits.GIEL = 1;
    INTCONbits.PEIE = 1;        // Enable global peripheral interrupts
    */
    ei();
    
}


// Sets the Grid 2 LED pins according to the provided byte
void setLEDGrid2(uint8_t setBits) {

    LATAbits.LATA0 = (setBits & (0x01)) ? 1 : 0;
    LATAbits.LATA1 = (setBits & (0x01 << 1)) ? 1 : 0;
    LATAbits.LATA2 = (setBits & (0x01 << 2)) ? 1 : 0;
    LATAbits.LATA3 = (setBits & (0x01 << 3)) ? 1 : 0;
    LATAbits.LATA4 = (setBits & (0x01 << 4)) ? 1 : 0;
    LATEbits.LATE0 = (setBits & (0x01 << 5)) ? 1 : 0;
    LATEbits.LATE1 = (setBits & (0x01 << 6)) ? 1 : 0;
    LATEbits.LATE2 = (setBits & (0x01 << 7)) ? 1 : 0;
            
}


// Sets the Grid 1 LED pins according to the provided byte
void setLEDGrid1(uint8_t setBits) {

    LATD = setBits;

}


// Sets the LED Layer pins according to the provided byte
void setLEDLayers(uint8_t setBits) {

    LATBbits.LATB5 = (setBits & 0x01) ? 1 : 0;
    LATBbits.LATB4 = (setBits & (0x01 << 1)) ? 1 : 0;
    LATBbits.LATB3 = (setBits & (0x01 << 2)) ? 1 : 0;
    LATBbits.LATB2 = (setBits & (0x01 << 3)) ? 1 : 0;
    
}

// Up to 999 microseconds delay.
void delay_ms(int ms) {

    int onesPlace = ms % 10;
    int remainder = ms / 10;
    int i = 0;

    for (i = 0; i < remainder; i++) {
        __delay_ms(10);
    }

    for (i = 0; i < onesPlace; i++)
        __delay_ms(1);

}


// Up to 5 seconds delay. The first digit gives the number of seconds. The
// second three give the number of micro seconds. Ex: 2345 provides 2 seconds,
// and 345 micro seconds delay.
void delay_sec(int delay) {

    int ms = delay % 1000;
    int sec = delay / 1000;
    int i = 0;

    for (i = 0; i < sec*100; i++ )
        __delay_ms(10);

    delay_ms(ms);
    
}

