/* 
 * File:   GlobalVars.h
 * Author: joshfadaie
 *
 * Created on June 23, 2014, 11:54 PM
 */

#ifndef GLOBALVARS_H
#define	GLOBALVARS_H


// ------------- GLOBAL VARIABLES ------------- //

// 1st dimension: Z Coordinate, 2nd dimension Y Coordinate, first 4 bits
// correspond to value of LEDs in X Dimension
volatile uint8_t cube[4][4];

volatile uint8_t cubeBuffer[4][4];

volatile uint8_t currentLayer = 0;   // The current layer being updated

int resetFlag = 0;  // Flag is set when cube has been reset

int seed = 0;   // seed value for random number generator


#endif	/* GLOBALVARS_H */

